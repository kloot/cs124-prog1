/****************************************************************************
* prim.c
*
* Computer Science 124
* Programming Assignment 1
*
* Implements functionality of Prim's algorithm.
***************************************************************************/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "graph.h"
#include "heap.h"

double prim(node *graph[], int n, int debug)
{
    // starting node
    int s = 0;

    // define distance array, array to define set S
    double dist[n];
    bool set[n];

    // construct, initialize heap
    heapNode h[n];
    int vertArray[n];
    int d = 2;
    initializeHeap(h, n);
    initializeVertexArray(vertArray, n);

    // insert first element of heap
    heapNode first;
    first.vertex = s;
    first.priority = 0;
    insert(first, h, vertArray, d);

    // initialize arrays
    for (int i = 0; i < n; i++)
    {
        dist[i] = INFTY;
        set[i] = 0;
    }

    // set dist of starting node to 0
    dist[s] = 0;

    // iterate until heap is empty
    while (!heapEmpty())
    {
        // get the lowest member of heap
        int v = deletemin(h, vertArray, d).vertex;

        // add this vertex to set S
        set[v] = true;

        node *ptr = graph[v];

        // find all (v,w) in E s.t. w in V - S
        while (ptr != NULL)
        {
            int w = ptr->vertex;
            double length = ptr->weight;

            // if vertex w is in V - S
            if (!set[w])
            {
                // update if length is less than recorded distance
                if (dist[w] > length)
                {
                    // record better distance
                    dist[w] = length;

                    // insert into heap
                    heapNode new = {.vertex = w, .priority = length};
                    insert(new, h, vertArray, d);
                }
            }
            ptr = ptr->next;
        }
    }

    // calculate total weight of MST
    double total = 0;
    double max = 0;

    // calculate maximum edge weight if debug is 4
    bool find_total = (debug != 4);
    for (int i = 0; i < n; i++)
    {
        // if distance was not updated from infinity, algorithm failed
        if (dist[i] > INFTY - 1)
            return -1.0;
        if (find_total)
            total += dist[i];
        else
            max = (dist[i] > max) ? dist[i] : max;
    }

    return (find_total) ? total : max;
}
