/****************************************************************************
* graph.c
*
* Computer Science 124
* Programming Assignment 1
*
* Implements a graph's functionality.
***************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <math.h>
#include "graph.h"

#define EPS 0.001

/*
 * Calculates distance between two vertices.
 */

double dist(double v1[], double v2[], int dimension)
{
    double x = 0;
    for (int i = 0; i < dimension; i++)
        x += (v1[i] - v2[i]) * (v1[i] - v2[i]);
    return sqrt(x);
}

/*
 * Adds undirected edge from v1 to v2 with specified weight.
 */

void add_edge(int v1, int v2, double weight, node *graph[]) 
{
    // insert v2 as neighbor of v1
    node *ptr = graph[v1];
    node *new_node = malloc(sizeof(node));
    new_node->vertex = v2;
    new_node->weight = weight;
    new_node->next = NULL;

    if (ptr == NULL)
        graph[v1] = new_node;
    else
    {
        new_node->next = graph[v1];
        graph[v1] = new_node;
    }
    return;
}

/*
 * Adds 'n' nodes of the specified dimension.
 */

bool add_nodes(int dimension, int n, node *graph[])
{
    // calculate maximum weight of edges to include
    int d = (dimension == 0) ? 2 : dimension;
    double k_of_n = (n <= 128) ? INFTY : 4.0 / pow(n, 1.0 / d);

    // ensure specified dimension is valid
    if (dimension == 1 || dimension > 4)
        return false;

    else if (dimension == 0)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < i; j++)
            {
                // add random edge weights
                double weight = rand() / (double)RAND_MAX;
                if (weight < k_of_n)
                {
                    add_edge(i, j, weight, graph);
                    add_edge(j, i, weight, graph);
                }
            }
        }
    }
    // dimension is 2, 3, or 4
    else
    {
        // array to store coordinates
        double vertices[n][dimension];

        // add n vertices
        for (int i = 0; i < n; i++)
        {
            // generate random coordinates
            for (int j = 0; j < dimension; j++)
                vertices[i][j] = rand() / (double) RAND_MAX;

            // calculate weights
            for (int j = 0; j < i; j++)
            {
                double weight = dist(vertices[i], vertices[j], dimension);

                // add edge to graph, if weight > k(n), do not add
                if (weight < k_of_n)
                {
                    add_edge(i, j, weight, graph);
                    add_edge(j, i, weight, graph);
                }
            }
        }
    }
    return true;
}

/*
 * Tests that two values are within EPS of each other.
 */

bool assert_equal(double x, double y)
{
    double diff = x - y;
    return (diff > EPS || diff < -EPS) ? false : true;
}

/*
 * Verifies graph functionality.
 */

bool test_graph(void)
{
    bool pass = true;
    double v1[2] = {0.0, 0.0};
    double v2[2] = {1.0, 1.0};
    double v3[2] = {0.3, 0.4};
    pass = assert_equal(dist(v1, v2, 2), sqrt(2));
    pass = assert_equal(dist(v1, v3, 2), 0.5);

    double v4[3] = {0.0, 0.0, 0.0};
    double v5[3] = {1.0, 1.0, 1.0};
    double v6[3] = {0.1, 0.2, 0.2};
    pass = assert_equal(dist(v4, v5, 3), sqrt(3));
    pass = assert_equal(dist(v4, v6, 3), 0.3);

    double v7[4] = {0.0, 0.0, 0.0, 0.0};
    double v8[4] = {1.0, 1.0, 1.0, 1.0};
    double v9[4] = {0.03, 0.04, 0.12, 0.84};
    pass = assert_equal(dist(v7, v8, 4), sqrt(4));
    pass = assert_equal(dist(v7, v9, 4), 0.85);
    pass = assert_equal(dist(v8, v9, 4), 1.63172);
    return pass;
}

/*
 * Helper for print_graph.
 */

 void print_list(node *ptr)
{
    if (ptr == NULL)
        return;
    else
    {
        printf("   %d (%f)\n", ptr->vertex, ptr->weight);
        print_list(ptr->next);
    }
}

/*
 * Prints graph.
 */

void print_graph(node *graph[], int n)
{
    for (int i = 0; i < n; i++)
    {
        printf("Vertex %d:\n", i);
        print_list(graph[i]);
    }
    return;
}

void free_graph(node *graph[], int n)
{
    // iterate over entire graph
    for(int i = 0; i < n; i++)
    {
        // store ptr
        node *ptr = graph[i];
        
        // traverse linked list, freeing each element
        while(ptr != NULL)
        {
            node *next = ptr->next;
            free(ptr);
            ptr = next;
        }
    }
    return;
}
