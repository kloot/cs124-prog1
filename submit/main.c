/***************************************************************************
* main.c
*
* Computer Science 124
* Programming Assignment 1
*
* Runs Prim's algorithm, print running times and graph weights.
*
* Usage: randmst debug numpoints numtrials dimension
*
* Debug: 0 = final results only
* 1 = run tests
* 2 = print graph
* 3 = print individual trial detail
* 4 = calculate maximum edge weight
***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "graph.h"
#include "heap.h"
#include "prim.h"

int main(int argc, char const *argv[])
{
    // error checking
    if (argc != 5)
    {
        printf("Usage: randmst debug numpoints numtrials dimension\n");
        return -1;
    }
    int debug = atoi(argv[1]);
    int n = atoi(argv[2]);
    int num_trials = atoi(argv[3]);
    int dimension = atoi(argv[4]);
    if (n < 0 || num_trials < 0 || dimension < 0)
    {
        printf("All parameters must be non-negative\n");
        return -1;
    }

    // testing
    if (debug > 0)
    {
        if(!test_graph())
        {
            printf("Test failed...aborting\n");
            return -2;
        }
        //testHeap(2);
    }

    double avg_weight = 0;
    double max_weight = 0;
    float avg_time = 0;
    int failures = 0;

    // set the seed for the rand function using the current time
    srand((unsigned int) time(NULL));

    // run num_trials
    for (int i = 0; i < num_trials; i++)
    {
        // timing
        clock_t t0, t1;

        t0 = clock(); // start clock

        // initialize graph, set pointers to NULL
        node *graph[n];
        memset(graph, 0, n * sizeof(node *));

        // populate graph
        if(!add_nodes(dimension, n, graph))
        {
            printf("dimension must be 0, 2, 3, 4\n");
            return -3;
        }
        
        t1 = clock(); // end clock

        // print graph if debug is 2
        if (debug == 2) print_graph(graph, n);
        if (debug == 3) printf("Graph constructed, running Prim's...\n");
        if (debug == 3) printf("Time to construct: %f\n", (float) (t1 - t0) / CLOCKS_PER_SEC);


        t0 = clock(); // start clock
        double prim_result = prim(graph, n, debug);
        t1 = clock(); // end clock

        if (prim_result > 0)
        {
            if (debug == 3) printf("Tree weight: %f\n", prim_result);
            if (debug == 3) printf("CPU time:    %f\n", (float) (t1 - t0) / CLOCKS_PER_SEC);
            
            // calculate maximum edge weight if debug is 4
            if (debug != 4)
                avg_weight += prim_result;
            else if (prim_result > max_weight)
                max_weight = prim_result;
            
            avg_time += (float) (t1 - t0) / CLOCKS_PER_SEC;
        }
        else
        {
            if (debug == 3) printf("Failure! Prim's did not find tree.\n");
            failures++;
        }

        // free memory used
        free_graph(graph, n);
    }

    if (debug != 4 && debug > 0)
    {
        printf("Average Tree weight: %f\n", avg_weight / num_trials);
        printf("Average CPU time:    %f\n", avg_time / num_trials);
        printf("Failures:            %d\n", failures);
    }
    if (debug == 4) printf("%f\n", max_weight);
    printf("%f %d %d %d\n", avg_weight / num_trials, n, num_trials, dimension);

    return 0;
}
