/****************************************************************************
* heap.h
*
* Computer Science 124
* Programming Assignment 1
*
* Declares our heap's functionality.
***************************************************************************/

#include <stdbool.h>

// our priority queue nodes
typedef struct
{
    int vertex;
    double priority;
} heapNode;

/*
 * Insert a node into a d-ary heap at position nodeNumber
 */
 
void initializeVertexArray(int vertarray[], int size);

/*
 * Initialize our heap
 */
void initializeHeap(heapNode heap[], int heapSize);

/*
 * Insert a node into a d-ary heap at position nodeNumber
 */
void insert(heapNode vertex, heapNode heap[], int vertarray[], int d);

/*
 * Delete the min of a d-ary heap of heapSize
 */
heapNode deletemin(heapNode heap[], int vertarray[], int d);
/*
 * Get the specified child of a node in a d-size d-ary heap
 */
int getChild(int nodeNumber, int child, int d);

/*
 * Get the parent of a node in a d-size d-ary heap
 */
int getParent(int nodeNumber, int d);

/*
 * Test if bool is empty
 */

bool heapEmpty();

/*
 * Test a heap
 */
 
void testHeap(int d);

/*
 * Print the heap
 */
void printHeap(heapNode heap[], int heapSize);
