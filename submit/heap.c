/****************************************************************************
* heap.c
*
* Computer Science 124
* Programming Assignment 1
*
* Implements our heap's functionality.
***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "heap.h"
#include <stdbool.h>


// global for the size of our heap atm
int heapCount = 0;

/*
 * Initialize vertex array to -1 for all
 */
 
void initializeVertexArray(int vertarray[], int size)
{
    for(int i = 0; i < size; i++)
        vertarray[i] = -1;
}

/*
 * Initialize our heap -- blank means 30
 */

void initializeHeap(heapNode heap[], int heapSize)
{
    heapNode blank;
    blank.vertex = 30;
    blank.priority = 30;
    
    for(int i = 0; i < heapSize; i++)
        heap[i] = blank;
}

/*
 * Percolate up (replace paernts with children if parent is greater)
 */

void percolateUp(heapNode vertex, heapNode heap[], int vertarray[], int nodeNumber, int d)
{
    // get the array element for the parent
    int parentNumber = getParent(nodeNumber, d);

    // while the priority is greater than the parent
    while(heap[parentNumber].priority > vertex.priority)
    {
        // change parent position in our array
        vertarray[heap[parentNumber].vertex] = nodeNumber;
        
        // bubble down the parent
        heap[nodeNumber] = heap[parentNumber];
        
        // new location for our vertex
        nodeNumber = parentNumber;

        // insert our vertex into the higher location
        heap[nodeNumber] = vertex;
            
        // new parent of the vertex
        parentNumber = getParent(nodeNumber, d);
    }

    // update vertex array
    vertarray[vertex.vertex] = nodeNumber;

    // set the correct position of the heap to the vertex
    heap[nodeNumber] = vertex;
}

/*
 * Test if bool is empty
 */

bool heapEmpty()
{
    return !heapCount;
}

void insert(heapNode vertex, heapNode heap[], int vertarray[], int d)
{
    // if vertex is not in array
    if(vertarray[vertex.vertex] == -1)
    {
        percolateUp(vertex, heap, vertarray, heapCount, d);
        heapCount++;
    }
    
    // if vertex is in the array
    else 
    {
        // if the priority is lower than parent, percolate
        if(vertex.priority < heap[getParent(vertarray[vertex.vertex], d)].priority)
            percolateUp(vertex, heap, vertarray, vertarray[vertex.vertex], d);
        // else just change the priority
        else
            heap[vertarray[vertex.vertex]].priority = vertex.priority;
    }
}

/*
 * Delete the min of a d-ary heap of heapSize
 */
heapNode deletemin(heapNode heap[], int vertarray[], int d)
{
    // define our blank node as 30's
    heapNode blank;
    blank.vertex = 30;
    blank.priority = 30;
    
    // if our heapcount is empty, return a blank node
    if(heapCount == 0)
        return blank;
 
    // subtract from heapCount to get correct new/last position
    heapCount--;   

	// the last node will start at location 0
    heapNode lastNode = heap[heapCount];

    // if it is the last in our array, set our array back to 0 
    if(heapCount == 0) 
    {
        heap[0] = blank;
        return lastNode;      
    }

    // set the last position to blank now; start at 0                        
    heap[heapCount] = blank;
    int childNumber;
    int heapNumber = 0;
    int smallChild = heapCount; 

    // what our min was
    heapNode min = heap[0];

    // bool to see if we found a child
    bool perc = true;
    
    // while we still find children
    while(perc)
    {
        // set perc as false
        perc = false;
        
        // for every child of our current node
        for(int i = 1; i <= d; i++)
        {
    	    // get the next child of the node
   		    childNumber = getChild(heapNumber, i, d);
            
            // if not a valid child, break
            if(childNumber > heapCount)
                break;
        
   		    // if the node has a higher priority than the child
    	    if(lastNode.priority > heap[childNumber].priority)
    	    {
                // if we are on the first child, or if the new child is smaller than the last
                if(i == 1 || heap[childNumber].priority < heap[smallChild].priority)
                smallChild = childNumber;
                perc = true;
    	    }
        }
    
        // if we made a swap
        if(perc)
        {
            // set the array for our child
            vertarray[heap[smallChild].vertex] = heapNumber;
        
            // swap positions
            heap[heapNumber] = heap[smallChild];
            heapNumber = smallChild;
            heap[smallChild].priority = lastNode.priority;
        }
    }

    // when we have our final position, set it and set the array position
    heap[heapNumber] = lastNode;
    vertarray[lastNode.vertex] = heapNumber;
    return min;
}

/*
 * Get the specified child of a node in a d-size d-ary heap
 */

int getChild(int heapNode, int child, int d)
{
    return d * heapNode + child;
}

/*
 * Get the parent of a node in a d-size d-ary heap
 */

int getParent(int heapNode, int d)
{
	return (heapNode - 1) / d;
}

/*
 * Print the heap
 */

void printHeap(heapNode heap[], int heapSize)
{
    // if heap is empty
    if(heap[0].vertex == 30)
        printf("Empty Heap");
	
    // else print all values
    for (int i = 0; i <= heapSize; i++)
        if (heap[i].vertex < 30)
            printf("{%d, %f} ", heap[i].vertex, heap[i].priority);
}

/*
 * Test a heap
 */
 
void testHeap(int d)
{
    int n = 10;
    heapNode heap[n];
    int vertArray[n];
    initializeHeap(heap, n);
    initializeVertexArray(vertArray, n);
    
    heapNode v[n];
    for (int i = 0; i < n; i++)
    {
    	v[i].vertex = i;
    	v[i].priority = i;
    }    

    insert(v[5], heap, vertArray, d);
    printHeap(heap, n);
    insert(v[2], heap, vertArray, d);
    printHeap(heap, n);
    insert(v[7], heap, vertArray, d);
    printHeap(heap, n);
    insert(v[9], heap, vertArray, d);   
    printHeap(heap, n);
    insert(v[1], heap, vertArray, d);
    printHeap(heap, n);
    insert(v[3], heap, vertArray, d);
    printHeap(heap, n);
    insert(v[8], heap, vertArray, d);
    printHeap(heap, n);
    insert(v[0], heap, vertArray, d);
    printHeap(heap,  n);
    insert(v[4], heap, vertArray, d);
    printHeap(heap, n);
    insert(v[6], heap, vertArray, d);
    printHeap(heap, n);
    v[6].priority = 0.5;
    insert(v[6], heap, vertArray, d);
    printHeap(heap, n);

    deletemin(heap, vertArray, d);
    printHeap(heap, n);
    deletemin(heap, vertArray, d);
    printHeap(heap, n);
    deletemin(heap, vertArray, d);
    printHeap(heap, n);
    deletemin(heap, vertArray, d);
    printHeap(heap, n);
    deletemin(heap, vertArray, d);
    printHeap(heap, n);
    deletemin(heap, vertArray, d);
    printHeap(heap, n);
    deletemin(heap, vertArray, d);
    printHeap(heap, n);
    deletemin(heap, vertArray, d);
    printHeap(heap, n);
    deletemin(heap, vertArray, d);
    printHeap(heap, n);
    deletemin(heap, vertArray, d);
    printHeap(heap, n);
    deletemin(heap, vertArray, d);
    printHeap(heap, n);
}
