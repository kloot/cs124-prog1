/****************************************************************************
* prim.h
*
* Computer Science 124
* Programming Assignment 1
*
* Declares functionality of Prim's algorithm.
***************************************************************************/

double prim(node *graph[], int n, int debug);
