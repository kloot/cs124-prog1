/****************************************************************************
* graph.h
*
* Computer Science 124
* Programming Assignment 1
*
* Declares a graph's functionality.
***************************************************************************/

#include <stdbool.h>
#define INFTY 9999.

// nodes for edges in adjacency list
typedef struct node
{
    int vertex;
    double weight;
    struct node *next;
} node;

/*
 * Adds 'n' nodes of the specified dimension. Returns false on failure.
 */
bool add_nodes(int dimension, int n, node *graph[]);

/*
 * Verifies graph functionality.
 */
bool test_graph(void);

/*
 * Prints graph.
 */
void print_graph(node *graph[], int n);

/*
 * Frees memory used by graph.
 */
void free_graph(node *graph[], int n);
