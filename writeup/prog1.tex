\documentclass[solution, letterpaper]{prog_asst}
\usepackage{dsfont}
\usepackage{algorithmic}
\usepackage{tikz}
\usepackage{setspace}
\usetikzlibrary{arrows}

%%%%%%%%%%%%%
% These are a few commands which I have found useful over the years..
%%%%%%%%%%%%%
\newcommand{\norm}[1]{\left|\left|#1\right|\right|}
\newcommand{\abs}[1]{\left|#1\right|}
\newcommand{\bx}{\mathbf{x}}
\newcommand{\bu}{\mathbf{u}}
\newcommand{\floor}[1]{\lfloor #1 \rfloor}
\newcommand{\ceil}[1]{\lceil#1 \rceil}
\newcommand{\bv}{\mathbf{v}}
\newcommand{\bbZ}{\mathds{Z}}
\newcommand{\bbR}{\mathds{R}}
\newcommand{\bbC}{\mathds{C}}
\newcommand{\bbN}{\mathds{N}}
\newcommand{\bbQ}{\mathds{Q}}
\newcommand{\bbzw}{\mathds{Z}[\omega]}
\newcommand{\bbzi}{\mathds{Z}[i]}
\newcommand{\nZ}[1][n]{\ensuremath{\mathds{Z}/#1\mathds{Z}}}
\newcommand{\<}{\langle}
\renewcommand{\>}{\rangle}

%% Please fill in your name and collaboration statement here.
\newcommand{\studentName}{Bannus Van der Kloot and Paul Bowden}
\newcommand{\className}{Computer Science 124}

\setstretch{1.5}

\begin{document}
\begin{singlespace}
\header{1}{Mar 1, 2012}
\end{singlespace}
\section{Code Design}

The first problem that we were presented with for this assignment was deciding which MST algorithm to use. We were deciding between Prim's and Kruskal's algorithms, as they were the two presented in class, and implementations of either did not seem too complicated. As we were required to scale the problem to large values of $n$ (32768), running time was a big consideration.

Prim's algorithm runs in $O(E \log V)$ using a binary heap for the priority queue and an adjacency list, while the dominant term in Kruskal's algorithm is sorting the edges, resulting in an $O(E \log E)$ runtime. Although there was a hint given about the ability to eliminate edges, and although $O(E \log E)$ only differs from $O(E \log V)$ at worst by a constant factor, the fact that we were dealing with a complete graph led us to settle upon Prim's algorithm.  We also made it possible to do a d-ary heap for any d value input in prim's.  While things worked somewhat faster with a different value of d than 2, our calculations and graphs ultimate display a binary heap.

Graph representation was also a concern. We decided against an adjacency matrix as storing up to 32768$\times$32768 `double' edge weights would require an 8 GB matrix. We decided on an adjaceny list because of this, along with the fact that Prim's algorithm works well with an adjacency list.

\section{Finding $k(n)$}
Per the hint, we searched for some function $k(n)$, such that throwing away edges with weight > $k(n)$ did not break our ability to find the minimum spanning tree. We know that throwing away edges larger than some $k(n)$ leaves us with two options after running an MST algorithm: either we find a valid MST, or we fail to find a tree at all. We can prove this simply by induction. The MST algorithm works for original tree. Then, assuming it works for some $k(n)$, if we lower $k(n)$ so that one set of edges that are all the same length are eliminated, we know that either the MST algorithm will still work, or we will have left some nodes unreachable. If a node was still reachable, but deleting edge(s) to it changed our MST, than this MST would not have been minimal, as the remaining edges from this node must be smaller than the edge(s) we just removed.

At first, to find this value, we simply changed a hard-coded value in \verb!add_nodes()! of \verb!graph.c!. We wrote our \verb!main()! function so that when running multiple trials, it would report any failures of Prim's to find an MST. Therefore, we could change this hard coded value and see at what point we were very unlikely to see any failures. We gathered the data for several data points for the 2-dimensional case, and then used \textit{Mathematica} to analyze the data. At first, we suspected some sort of inverse function, so adding another negative power of $x$, we got this curious Taylor-series-resembling function:

\includegraphics[width=358px]{2d-1.png}

A different \textit{Mathematica} function yielded better results:

\includegraphics[width=359px]{2d-2.png}

Being so close to $\frac{c}{\sqrt{x}}$ (for some constant $c$), we tested how closely this modeled the data:\\

\includegraphics[width=358px]{2d-3.png}

This seemed to be a satisfactory answer. The guess and check process was a little time consuming, so to analyze this for other dimensions, we decided to modify our program so that it could also return the longest edge used in the MST. Using the data from this program in \textit{Mathematica}, we modeled for best fit. Noting that we rounded up a bit to get to $\sqrt{x}=x^{-1/2}$ for dimension 2, we rounded to get $x^{-1/3}$ for dimension 3 and $x^{-1/4}$ for dimension 4, which leads to a nice correspondence between dimensions and $k(n)$. Dimension 0 was close enough to $x^{-1/2}$ that it seemed reasonable to use it. Graphics for these tests are at the end of the file. None of the constants were above 3, so we decided to use a constant of 4 as a safe upper bound. Our final function:

\[
   k(n) = \left\{
     \begin{array}{ll}
       4 x^{-1/2} & : \text{dimension} = 0\\
       4 x^{-1/d} & : \text{other dimensions}
     \end{array}
   \right.
\]

\section{Determining $f(n)$}
After having implemented our algorithm, we ran it on the given values of $n$ to determine what the growth rates of MST sizes are for different dimensions. The results were all collected on Bannus's MacBook. We have decided to depict the data on a graph along with our best guess for $f(n)$, using \textit{Mathematica}:

\subsection{Random weights (Dimension 0)}
Here, starting at $n=16$, we are already approaching an asymptote of about 1.2. For smaller values, one can see the progression of this function to this value, but for 16 and above, it seems to be constant $f(n)=1.20425$.

\includegraphics[width=358px]{fn0d.png}

\subsection{Dimension 2}

For the next three $f(n),$ we were able to use \verb!FindFit! with $f(n)=a x^b$ to get functions with parameters that matched our data extremely closely. Here, we found that $f(n) = 0.684 n^{0.495}$ was very good.

\includegraphics[width=358px]{fn2d.png}

\subsection{Dimension 3}

For the third dimension, we got $f(n) = 0.595 n^{0.660}$.

\includegraphics[width=358px]{fn3d.png}

\subsection{Dimension 4}

For the fourth dimension, we got $f(n) = 0.764 n^{0.741}$.

\includegraphics[width=358px]{fn4d.png}

Interestingly, each of these exponents was slightly under $d-1/d$. If we had to guess at a general trend, it would seem this pattern would be likely to extend to the higher dimensions. The most surprising graph here is that of $f(n)$ for the 0th dimension. It stays flat across 1.20535 for any added vertexes.  While this may seem surprising at first, we weigh this against the fact that any added nodes must be close to every other existing node -- eventually, there is no real chance to increase the tree any significant distance more.

\section{Performance}
Our algorithm runs very fast -- while the program takes about 12 seconds to complete on 32768 vertexes (on nice), the algorithm takes mere tenths of a second.  This makes sense, as the creating of the graph takes $O(V^2)$ time, and even though we are cutting down a lot using a $k(n)$ value to eliminate edges, it does not cut it down significantly enough to that of our algorithm running in $O(E \log E)$.

Our program is capable of running with more than 200,000 vectors before seg faulting, likely because we run out of heap space for mallocing while creating the graph.  

The program itself may end up having issues with caching in the upper echelon of vertex values during things like graph creation, but we notice no significant point at which we have a significant jump in running time for our algorithm -- Prim's itself does not rely heavily on caching.

We did run into some odd optimization bugs. Using the -O3 tag, on Bannus's MacBook, the program would infinitely loop on input like \verb!./randmst 0 2 10 0!, and seg fault on \verb!./randmst 0 3 10 0!. We investigated this, and seemingly gcc was optimizing out a loop counter. It seemingly worked on the Harvard system (nice), so we kept the optimization in. If in testing, any problems are encountered, removing the optimization would be recommended.

The data for running times of Prim's in the various dimensions fit closely with polynomial functions. For dimensions 2, 3, and 4, the degree of the polynomial causes the function to behave very similarly to $n \log n$, while for 0, it is performing worse than this. We were at a loss to explain such behavior. The graphs for these running times are appended at the end.

\section{Other graphics}

\subsection{0d $k(n)$ calculation}
\includegraphics[width=358px]{0d.png}
\subsection{3d $k(n)$ calculation}
\includegraphics[width=358px]{3d.png}
\subsection{4d $k(n)$ calculation}
\includegraphics[width=358px]{4d.png}

\subsection{0d running time}
\includegraphics[width=358px]{rt-0d.png}
\subsection{2d running time}
\includegraphics[width=358px]{rt-2d1.png}

\includegraphics[width=358px]{rt-2d2.png}
\subsection{3d running time}
\includegraphics[width=358px]{rt-3d1.png}

\includegraphics[width=358px]{rt-3d2.png}
\subsection{4d running time}
\includegraphics[width=358px]{rt-4d1.png}

\includegraphics[width=358px]{rt-4d2.png}


\end{document}
